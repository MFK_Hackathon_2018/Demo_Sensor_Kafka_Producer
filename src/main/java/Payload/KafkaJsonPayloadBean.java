package Payload;

import java.util.List;

public class KafkaJsonPayloadBean {
    private String dataType;
    private String model;
    private String source;
    private long timestamp;
    private List<Pair> fields;

    public KafkaJsonPayloadBean(String dataType, String model, String source, long timestamp, List<Pair> fields) {
        this.dataType = dataType;
        this.model = model;
        this.source = source;
        this.timestamp = timestamp;
        this.fields = fields;
    }
}