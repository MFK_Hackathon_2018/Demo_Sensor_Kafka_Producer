import Payload.KafkaJsonPayloadBean;
import Payload.Pair;
import com.google.gson.Gson;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class DemoSensor {
    private final static String TOPIC = "testTopic";
    private final static String BOOTSTRAP_SERVERS =
            "kafka:9092";

    private static Producer<Long, String> createProducer() {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                BOOTSTRAP_SERVERS);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "DemoSensor");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                LongSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class.getName());
        props.put(ProducerConfig.RETRIES_CONFIG, 5);

        return new KafkaProducer<>(props);
    }

    private static double STARTING_LATITUDE = 38.623385;
    private static double STARTING_LONGITUDE = -90.197267;
    private static double LAT_LONG_DEVIATION = .001;
    private static String LAT_LONG_FORMAT = "%.6f";

    public static void runProducer() {
        final Producer<Long, String> producer = createProducer();

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                long time = System.currentTimeMillis();

                String value = Integer.toString(ThreadLocalRandom.current().nextInt(0, 100));
                String lat = String.format(LAT_LONG_FORMAT, ThreadLocalRandom.current().nextDouble(STARTING_LATITUDE - LAT_LONG_DEVIATION, STARTING_LATITUDE + LAT_LONG_DEVIATION));
                String lon = String.format(LAT_LONG_FORMAT, ThreadLocalRandom.current().nextDouble(STARTING_LONGITUDE - LAT_LONG_DEVIATION, STARTING_LONGITUDE + LAT_LONG_DEVIATION));

                List<Pair> data = Arrays.asList(new Pair("cps", value), new Pair("latitude", lat), new Pair("longitude", lon));

                KafkaJsonPayloadBean payload = new KafkaJsonPayloadBean("sensor", "demoModel", "demoSource", time, data);

                String jsonPayload = new Gson().toJson(payload);
                final ProducerRecord<Long, String> record = new ProducerRecord<>(TOPIC, time, jsonPayload);
                try {
                    RecordMetadata metadata = producer.send(record).get(30, TimeUnit.SECONDS);
                    long elapsedTime = System.currentTimeMillis() - time;
                    System.out.printf(
                            "sent record(key=%s value=%s) " + "meta(partition=%d, offset=%d) time=%d\n",
                            record.key(), record.value(), metadata.partition(),
                            metadata.offset(), elapsedTime);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 0, 1000);
    }

    public static void main(String... arg) {
        runProducer();
    }
}