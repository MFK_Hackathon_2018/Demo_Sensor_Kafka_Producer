# Demo Sensor
- Sends a Json Payload of Fake Sensor Data to a Kafka Server
   - Sensor Data is a cps, lat, and lon
- Most things hardcoded (ie Topic, Kafka Server, etc)

#### TODO
- Extract beans to a common place to be used on both sides of Kafka Streams
- Refactor so its a bit more professional looking 
- Extract kafka producer code to be used between different integrations